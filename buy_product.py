import psycopg2

price_request = (
    "SELECT price "
    "FROM Shop "
    "WHERE product = %(product)s"
)
buy_decrease_balance = (
    f"UPDATE Player "
    f"SET balance = balance - ({price_request}) * %(amount)s "
    f"WHERE username = %(username)s"
)
buy_decrease_stock = (
    "UPDATE Shop "
    "SET in_stock = in_stock - %(amount)s "
    "WHERE product = %(product)s"
)
inventory_total_request = (
    "SELECT sum(amount) "
    "FROM Inventory "
    "WHERE username = %(username)s"
)
update_inventory = (
    "INSERT INTO Inventory(username, product, amount)"
    "VALUES (%(username)s, %(product)s, %(amount)s) "
    "ON CONFLICT ON CONSTRAINT Inventory_username_product_unique "
    "DO UPDATE SET amount = Inventory.amount + EXCLUDED.amount"
)


def get_connection():
    conn = psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port=5432
    )
    # just one of the ways to avoid write anomalies
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE)
    return conn


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation:
                raise Exception("Product is out of stock")

            try:
                cur.execute(inventory_total_request, obj)
                maybe_total = cur.fetchone()
                if maybe_total is None or maybe_total == (None,):  # no inventory for user
                    total = 0  # we will do upsert, so no insert is needed here
                else:
                    total = maybe_total[0]
                if total + amount > 100:
                    raise Exception("Total amount of product should be less or equal to 100")
            except psycopg2.errors.CheckViolation:
                raise Exception("Cannot update inventory")

            cur.execute(update_inventory, obj)
            conn.commit()


buy_product('Alice', 'marshmello', 1)
