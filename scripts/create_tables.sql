CREATE TABLE Player
(
    username TEXT,
    balance  INT
        CONSTRAINT Player_balance_check CHECK (balance >= 0),
    CONSTRAINT Player_username_unique UNIQUE (username)
);
CREATE TABLE Shop
(
    product  TEXT UNIQUE,
    in_stock INT
        CONSTRAINT Shop_in_stock CHECK (in_stock >= 0),
    price    INT
        CONSTRAINT Shop_price_check CHECK (price >= 0),
    CONSTRAINT Shop_product_unique UNIQUE (product)
);


CREATE TABLE Inventory
(
    id       serial PRIMARY KEY NOT NULL,
    username TEXT               NOT NULL REFERENCES Player (username),
    product  TEXT               NOT NULL REFERENCES Shop (product),
    amount   INT
        CONSTRAINT Inventory_amount_check CHECK (amount >= 0),
    CONSTRAINT Inventory_username_product_unique UNIQUE (username, product)
);